const { Schema, model } = require("mongoose");
const validateEmail = function (email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};
// Schema domyślnie dodaje unikalne pole _id, dlatego pomijamy je w deklaracji
const userSchema = new Schema({
  login: { type: String, required: true },
  email: {
    type: String,
    require: true,
    validate: [validateEmail, "Please fill a valid email address"],
  },
  registrationDate: Date,
});

module.exports = model("User", userSchema);
