const express = require("express");
const router = express.Router();

const User = require("../models/User");

router.get("/", async (req, res) => {
  try {
    const user = await User.find();
    return res.send({
      allUsers: user,
    });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.post("/", async (req, res) => {
  const user = new User({
    login: req.body.login,
    email: req.body.email,
    registrationDate: req.body.registrationDate,
  });
  user
    .save()
    .then((result) => {
      console.log(result);
      res.status(201).json({
        createdUser: result,
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err.message,
      });
    });
});

router.get("/:id", async (req, res) => {
  try {
    const user = await User.find({ _id: req.params.id });
    return res.send({ user });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
});

router.put("/:id", async (req, res, next) => {
  const id = req.params.id;
  User.findOneAndUpdate({ _id: id }, req.body, {
    useFindAndModify: false,
  }).then((user) => {
    User.findOne({ _id: id })
      .then((user) => {
        res.send({
          putUserId: id,
        });
      })
      .catch(next);
  });
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  User.deleteOne({ _id: id }, function (err, user) {
    if (err) return res.status(400).send(err);
    if (user.deletedCount > 0) {
      return res.send({
        deletedUserId: id,
      });
    } else {
      return res.status(404).send("Does not exists");
    }
  });
});
router.patch("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    await User.updateOne({ _id: id }, { $set: req.body });
    return res.send({
      patchUserId: id,
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

module.exports = router;
